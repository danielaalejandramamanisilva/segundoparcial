package segundoparcial;
/**
 *
 * @author Nombre: Daniela Alejandra Mamani Silva  C.I.: 8537754
 */
public class nodo<T> {
    private T element;
    private nodo<T> next;
    
    public nodo(T elemento, nodo<T> siguiente){//creamos nuestro constructor
        this.element = element;
        this.next = next;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public nodo<T> getNext() {
        return next;
    }

    public void setNext(nodo<T> next) {
        this.next = next;
    }
    
}
