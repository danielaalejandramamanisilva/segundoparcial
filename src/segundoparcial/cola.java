package segundoparcial;
/**
 *
 * @author Nombre: Daniela Alejandra Mamani Silva  C.I.: 8537754
 */
public class cola <T>{
    private nodo<T> first;
    private nodo<T> last;
    private int size;
    private int limit;
    
    public cola(){
        this.first = null;
        this.last = null;
        size = 0;
        this.limit=-1;
    }
     public void setLimit(int limit){
        this.limit=limit;
    }
     
    public boolean isLimit(){
        return size==limit;
    }
    
    public boolean empty(){
        return first == null;
    }
    
    public int sizeCola(){
        return size;
    }
    
    public T peek(){
        if(empty()){
            return null;
        }
        return first.getElement();
    }
    
    public void push(T element){
        nodo<T> newElement = new nodo(element,null);
        if(empty()){
            first = newElement;
            last = newElement;
        }else{
            if(sizeCola() == 1){
                first.setNext(newElement);
            }else{
                last.setNext(newElement);
            }
            last = newElement;
        }
        size++;
    }
    
    public T pop(){
        if(empty()){
            return null;
        }
        
        T element = first.getElement();
        nodo<T> aux = first.getNext();
        first = aux;
        size--;
        if(empty()){
            last = null;
        }
        return element;
    }
}
